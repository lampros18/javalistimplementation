package com.company;

public class Node extends ListItem {
    public Node() {
    }

    @Override
    public int compareTo(Data data1, Data data2) {
        if(data1.getStreetNumber() >= data2.getStreetNumber() )
        {
            return 1;
        }else if(data1.getStreetNumber() < data2.getStreetNumber())
        {
            return -1;
        }else {
            return 0; //error
        }
    }
}
