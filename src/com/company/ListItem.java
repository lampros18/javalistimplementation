package com.company;

public abstract class ListItem {

    private Data data;
    private ListItem next;
    private ListItem previous;

    public ListItem() {
        this.data = null;
        this.next = null;
        this.previous = null;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public ListItem getNext() {
        return next;
    }

    public void setNext(ListItem next) {
        this.next = next;
    }

    public ListItem getPrevious() {
        return previous;
    }

    public void setPrevious(ListItem previous) {
        this.previous = previous;
    }

    public abstract int compareTo(Data data1 , Data data2);
}
