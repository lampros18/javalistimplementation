package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        SList sList = new SList();
        //This may vary on the computer the code runs
        String filePath = "/home/hades/IdeaProjects/AbstractClassesChallenge/src/com/company/dataset.csv";

        File file = new File(filePath);

        try {

            Scanner scanner = new Scanner(file);

            scanner.useDelimiter("\n");
            scanner.next();
            while(scanner.hasNext())
            {
                String data = scanner.next();

                String[] value = data.split(",");
                sList.addItem(new Data(Integer.parseInt(value[0]),value[1],value[2],Double.parseDouble(value[3]),Double.parseDouble(value[4]),value[5]));
            }
            scanner.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        sList.printList();
        System.out.println(sList.size());
        sList.removeItem(99621);
        sList.printList();
        System.out.println(sList.size());
    }
}
