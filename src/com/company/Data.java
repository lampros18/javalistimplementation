package com.company;

public class Data {
    private int streetNumber;
    private String country;
    private String city;
    private double latitude;
    private double longitude;
    private String streetName;

    public Data(int streetNumber, String country, String city, double latitude, double longitude, String streetName) {
        this.streetNumber = streetNumber;
        this.country = country;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
        this.streetName = streetName;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getStreetName() {
        return streetName;
    }

    @Override
    public String toString() {
        return "Data{" +
                "streetNumber=" + streetNumber +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", streetName='" + streetName + '\'' +
                '}';
    }
}
