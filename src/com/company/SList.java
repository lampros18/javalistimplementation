package com.company;

import org.omg.PortableServer.LIFESPAN_POLICY_ID;

import java.util.Objects;

public class SList {

    private int size;
    private Head head;

    private class Head{

        private ListItem head;

        public Head() {
            this.head = null;
        }

        public ListItem getHead() {
            return head;
        }

        public void setHead(ListItem head) {
            this.head = head;
        }
    }

    public SList() {
        this.size = 0;
        this.head = new Head();
    }

    public ListItem getHead()
    {
        return this.head.getHead();
    }

    public void setHead(ListItem head)
    {
        this.head.setHead(head);
    }

    public int size(){return this.size;}

    public void addItem(Data data)
    {
        if (getHead() == null)
        {
            Node node = new Node();
            node.setData(data);
            node.setNext(null);
            node.setPrevious(null);
            setHead(node);
            size++;
        } else {
            ListItem current = getHead();

            if( current.compareTo(data,current.getData()) == -1)
            {// Add at the beginning of the lsit
                Node node = new Node();
                node.setData(data);
                node.setPrevious(null);
                node.setNext(current);
                current.setPrevious(node);
                setHead(node);
                size++;
            }

            if(current.compareTo(data,current.getData()) == 1)
            { //Search where to add the ListItem
                while ( current.getNext() != null && current.compareTo(data,current.getData()) == 1)
                {
                    current = current.getNext();
                }

                if(current.getNext() == null && current.compareTo(data,current.getData()) == 1)
                {//Add at the end of the list
                    Node node = new Node();
                    node.setData(data);
                    node.setNext(current.getNext());
                    node.setPrevious(current);
                    current.setNext(node);
                    size++;
                }

                if(current.compareTo(data,current.getData()) == -1)
                {//Add between two list items
                    Node node = new Node();
                    node.setData(data);
                    node.setNext(current);
                    node.setPrevious(current.getPrevious());
                    current.getPrevious().setNext(node);//This statement must executed first!!
                    current.setPrevious(node);
                    size++;
                }
                if(current.compareTo(data,current.getData()) == 0)
                {
                    System.out.println("Error occured");
                }

            }
        }
    }

    public void printList()
    {
        ListItem current = getHead();
        while(current != null)
        {
            System.out.println(current.getData().toString());
            current = current.getNext();
        }
    }

    public void removeItem(int number)
    {
        ListItem current = getHead();
        while ( current != null && !(current.getData().getStreetNumber() == number))
            current = current.getNext();
        if(current == getHead())
        {//Remove the first node
            setHead(current.getNext());
            head.getHead().getNext().setPrevious(null);
            free();
            size--;
            return;
        }
        if(current.getNext() == null)
        {//Remove the last node
            System.out.println(current.getPrevious().getData().getCountry());
            current.getPrevious().setNext(null);
            free();
            size--;
            return;
        }

        if(current != getHead() && current.getNext() != null)
        {//The node to be removed lies between two nodes
            current.getPrevious().setNext(current.getNext());
            current.getNext().setPrevious(current.getPrevious());
            current.setPrevious(null);
            current.setNext(null);
            free();
            size--;
        }
    }

    public void free()
    {
        try{
            Thread.sleep(500);
            System.gc();
        } catch ( InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void printListReverse()
    {
        ListItem current = getHead();
        while(current != null)
        {
            current = current.getNext();
            if(current.getNext() == null)
                break;
        }
        int m=0;
        while(current != null)
        {
            m++;
            System.out.println("R->" + current.getData().toString());
            current = current.getPrevious();
        }
        System.out.println("m->" + m);
    }

}
